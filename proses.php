<?php
$awal = $_POST['namaawal'];
$akhir = $_POST['namaakhir'];
$email = $_POST['email'];
$vga = $_POST['vga'];
$ram = $_POST['ram'];
$mobo = $_POST['mobo'];
$processor = $_POST['processor'];
$psu = $_POST['psu'];
$hdd = $_POST['hdd'];
$case = $_POST['case'];
$total = 0;

$hargavga = array("","-",11250000,6750000,4250000,2130000,1675000);
$namavga = array("","Tidak memilih","NVIDIA GTX 1080","NVIDIA GTX 1070","NVIDIA GTX 1060","NVIDIA GTX 1050 Ti","NVIDIA GTX 1050");
$namacpu = array("","Tidak Memilih","Intel Core i5 6500","Intel Core i5 6600","Intel Core i5 6600K","Intel Core i3 6100","Intel Pentium G4400","Intel Core i7 6700K");
$hargacpu = array("","-",2500000,2700000,2900000,1456000,785000,4650000);
$namaram = array("","Tidak Memilih","Corsair DDR4 Vengeance 8GB(2x4GB)","Corsair DDR4 Vengeance 4GB(2x2GB)","Corsair DDR4 Vengeance 8GB(1x8GB)","Corsair DDR4 Vengeance 4GB(1x4GB)");
$hargaram = array("","-",827500,425000,735000,380000);
$namamobo = array("","Tidak Memilih","Asus H110M-E (LGA1151)","Asus H170M-E D3 (LGA1151)","Asus B150M PRO GAMING (LGA1151)","Asus Sabertooth Z170 Mark 1 (LGA1151)");
$hargamobo = array("","-",900000,1895000,1520000,4175000);
$namapsu = array("","Tidak Memilih","Corsair CXM Series 450W Modular-Bronze","Corsair CXM Series 550W Modular-Bronze","Corsair CXM Series 650W Modular-Bronze","Corsair CXM Series 750W Modular-Bronze");
$namahdd = array("","Tidak Memilih","Intel SSD 120GB 540 Series","Intel SSD 240GB 535 Series","Intel SSD 480GB S3100 Series","WDC 1TB SATA3 64MB Black","WDC 500GB SATA3 32MB Black");
$hargahdd = array("","-",800000,1930000,3840000,1100000,864000);
$hargapsu = array("","-",860000,985000,1105000,1660000);
$namacase = array("","Tidak Memilih","Corsair Carbide SPEC","Corsair Obsidian 450X","Corsair Graphite 760T");
$hargacase = array("","-",955000,1735000,2685000);
$total = $hargavga[$vga] + $hargacpu[$processor] +$hargaram[$ram]+$hargamobo[$mobo]+$hargapsu[$psu]+$hargahdd[$hdd]+$hargacase[$case] + 2500;



 function Diskon($total){
        if ($total >= 10000000){
            $cek = "Anda mendapatkan promo membeli game GTA V Gratis!";
            return $cek;
        }
        else if($total >=8000000){
            $cek = "Anda mendapatkan promo membeli PES 2016 Gratis!";
            return $cek;
        }
        else if($total >=5000000){
            $cek = "Anda mendapatkan promo membeli CS:GO Gratis!";
            return $cek;
        }
     else{
         $cek = "Anda tidak mendapatkan apa apa";
         return $cek;
     }
        
    }
?>
    <html>

    <head>
        <title>Beranda</title>
        <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="icon" href="gambar/favicon.ico">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/java.js"></script>
        <style>
            #proses {
                margin-top: 50px;
                text-align: center;
                color: white;
                margin-left: 10px;
                margin-right: 10px;
            }
            
            .table {
                margin-left: 380px;
                width: 600px;
            }

        </style>
    </head>

    <body class="proses">
        <div class="header">
            <img src="gambar/logo1.png">
        </div>
        <div class="header2">
            <ul>
                <li><a href="index.php" class="dropbtn">Beranda</a></li>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Produk Kami</a>
                    <div class="dropdown-content">
                        <a href="nvidia.php">VGA</a>
                        <a href="intel.php">Processor</a>
                        <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group" style="margin-top:15px;">
                                <input type="text" class="form-control" placeholder="Search" id="search">
                            </div>
                            <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                        </form>
                    </div>
            </ul>
        </div>
        <div id="proses">
            <img src="gambar/logo1.png">
            <h2>Simulasi Harga</h2>
            <br>
            <p>
               Last Update <?= date("l jS \of F Y h:i:s A") ?>
            </p>
            <p>Hai
                <?= $awal ?>
                    <?= $akhir ?>
            </p>
            <p>Anda akan membeli daftar sebagai berikut:</p>
            <table class="table table-bordered">
                <tr>
                    <td>Jenis Produk</td>
                    <td>Merek Produk</td>
                    <td>Harga Produk</td>
                </tr>
                <tr>
                    <td>VGA</td>
                    <td>
                        <?= $namavga[$vga] ?>
                    </td>
                    <td>Rp.
                        <?= $hargavga[$vga] ?>
                    </td>
                </tr>
                <tr>
                    <td>Processor</td>
                    <td>
                        <?= $namacpu[$processor] ?>
                    </td>
                    <td>Rp.
                        <?= $hargacpu[$processor] ?>
                    </td>
                </tr>
                <tr>
                    <td>RAM</td>
                    <td>
                        <?= $namaram[$ram] ?>
                    </td>
                    <td>Rp.
                        <?= $hargaram[$ram] ?>
                    </td>
                </tr>

                <tr>
                    <td>Motherboard</td>
                    <td>
                        <?= $namamobo[$mobo] ?>
                    </td>
                    <td>Rp.
                        <?= $hargamobo[$mobo] ?>
                    </td>
                </tr>

                <tr>
                    <td>Power Supply</td>
                    <td>
                        <?= $namapsu[$psu] ?>
                    </td>
                    <td>Rp.
                        <?= $hargapsu[$psu] ?>
                    </td>
                </tr>
                <tr>
                    <td>Penyimpanan</td>
                    <td>
                        <?= $namahdd[$hdd] ?>
                    </td>
                    <td>Rp.
                        <?= $hargahdd[$hdd] ?>
                    </td>
                </tr>
                <tr>
                    <td>Casing</td>
                    <td>
                        <?= $namacase[$case] ?>
                    </td>
                    <td>Rp.
                        <?= $hargacase[$case] ?>
                    </td>
                </tr>
                <tr>
                    <td> Total </td>
                    <td></td>
                    <td>Rp.
                        <?= $total?>
                    </td>
                </tr>
            </table>
            <p>
                <?= Diskon($total) ?>
            </p>
            <p> Cetak bukti karena ini pembayaran yang sah</p>
            <p>Bukti pembayaran dikirim ke
                <?= $email ?> juga</p>
        </div>
    </body>

    </html>
