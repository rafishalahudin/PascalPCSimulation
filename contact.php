<html>

<head>
    <title>Beranda</title>
    <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="icon" href="gambar/favicon.ico">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fonts/style.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/java.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
    <script src="js/script.js" type="text/javascript"></script>
</head>


<body>
   <div class="header">
        <img src="gambar/logo1.png">
    </div>
    <div class="header2">
        <ul>
            <li><a href="index.php" class="dropbtn">Beranda</a></li>
            <li class="dropdown">
                <a href="#" class="dropbtn">Produk Kami</a>
                <div class="dropdown-content">
                    <a href="nvidia.php">VGA</a>
                    <a href="intel.php">Processor</a>
                    <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group" style="margin-top:15px;">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                        </div>
                        <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                    </form>
                </div>
        </ul>
    </div>

    <!-- Contact Form -->
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <h2>Info Contact</h2>
                <div class="info">
                    <h3>SMK Negeri 4 Bandung <br> Jl. Kliningan No. 6</h3>
                    <ul>
                        <li><span class="icon icon-facebook"></span></li>
                        <li>Pascal Electronics</li>
                    </ul>
                    <ul>
                        <li><span class="icon icon-instagram"></span></li>
                        <li>PascalElectronics</li>
                    </ul>
                    <div id="map" style="width:10%;height:50px"></div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <form>
                    <h2>Contact Form</h2>
                    <div class="half">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Alamat Email</label>
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Telepon</label>
                            <input type="text" class="form-control" placeholder="Telepon">
                        </div>
                        <div class="form-group">
                            <label>Pesan</label>
                            <textarea class="form-control" placeholder="Pesan"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>