<html>

<head>
    <title>Beranda</title>
    <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="icon" href="gambar/favicon.ico">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/java.js"></script>
</head>

<body class="ha">
    <div class="header">
        <img src="gambar/logo1.png">
    </div>
    <div class="header2">
        <ul>
            <li><a href="index.php" class="dropbtn">Beranda</a></li>
            <li class="dropdown">
                <a href="#" class="dropbtn">Produk Kami</a>
                <div class="dropdown-content">
                    <a href="nvidia.php">VGA</a>
                    <a href="intel.php">Processor</a>
                    <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group" style="margin-top:15px;">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                        </div>
                        <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                    </form>
                </div>
        </ul>
    </div>
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <form role="form" method="POST" action="proses.php">

                <fieldset>
                    <legend style="color: lawngreen;">Form Pembelian</legend>

                    <div class="form-group col-md-6" style="color: lawngreen;">
                        <label for="first_name">First name</label>
                        <input type="text" class="form-control" name="namaawal" id="first_name" placeholder="First Name" required>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="last_name" style="color: lawngreen;">Last name</label>
                        <input type="text" class="form-control" name="namaakhir" id="" placeholder="Last Name" required>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="" style="color: lawngreen;">Email</label>
                        <input type="text" class="form-control" name="email" id="" placeholder="Email" required>
                    </div>



                </fieldset>

                <fieldset>
                    <legend style="color: lawngreen;">Komponen PC</legend>

                    <div class="form-group col-md-12">
                        <label for="country" style="color: lawngreen;">VGA yang mau dibeli</label>
                        <select class="form-control" name="vga" id="country">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih VGA-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">NVIDIA GTX 1080 8GB DDR5X Triple Fan Founders Edition(Rp.11.250.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">NVIDIA GTX 1070 8GB DDR5 EXOC Founders Edition(Rp.6.750.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">NVIDIA GTX 1060 6GB DDR5 EXOC Founders Edition(Rp.4.250.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">NVIDIA GTX 1050 Ti OC 4GB DDR5 Founders Edition(Rp.2.130.000)</option>
                             <option style="background-color: black; color:lawngreen;" value="6">NVIDIA GTX 1050 2GB DDR5 dual Founders Edition(Rp.1.675.000)</option>
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">Processor yang mau dibeli</label>
                        <select class="form-control" name="processor" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih Processor-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Intel Core i5 6500 3.2Ghz Socket LGA 1151 Skylake Series (Rp.2.500.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Intel Core i5 6600 3.3 Ghz Socket LGA 1151 Skylake Series (Rp.2.700.000) </option>
                            <option style="background-color: black; color:lawngreen;" value="4">Intel Core i5 6600K 3.5 Ghz Socket LGA 1151 Skylake Series (Rp.2.900.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">Intel Core i3 6100 3.7Ghz Socket LGA 1151 Skylake Series(Rp.1.465.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="6">Intel Pentium G4400 3.3 Ghz Socket LGA 1151 Skylake Series (Rp.785.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="7">Intel Core i7 6700K 4.0 Ghz up to 4.2Ghz Socket LGA 1151 Skylake(Rp.4.650.000)</option>
                        </select>
                    </div>

                     <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">Motherboard yang mau dibeli</label>
                        <select class="form-control" name="mobo" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih Motherboard-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Asus H110M-E (LGA1151) (Rp.900.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Asus H170M-E D3 (LGA1151) (Rp.1.895.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">Asus B150M PRO GAMING (LGA1151) (Rp.1.520.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">Asus Sabertooth Z170 Mark 1 (LGA1151) (Rp.4.175.000)</option>
                        </select>
                    </div>

                    
                    <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">RAM yang mau dibeli</label>
                        <select class="form-control" name="ram" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih RAM-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Corsair DDR4 Vengeance 8GB(2x4GB) PC19200 (Rp.827.500)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Corsair DDR4 Vengeance 4GB(2x2GB) PC19200 (Rp.425.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">Corsair DDR4 Vengeance 8GB(1x8GB) PC19200 (Rp.735.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">Corsair DDR4 Vengeance 4GB(1x4GB) PC19200 (Rp.380.000)</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">PSU yang mau dibeli</label>
                        <select class="form-control" name="psu" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih PSU-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Corsair CXM Series 450W Modular-Bronze (Rp.860.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Corsair CXM Series 550W Modular-Bronze (Rp.985.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">Corsair CXM Series 650W Modular-Bronze (Rp.1.105.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">Corsair CXM Series 750W Modular-Bronze (Rp.1.660.000)</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">Penyimpanan yang mau dibeli</label>
                        <select class="form-control" name="hdd" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih Penyimpanan-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Intel SSD 120GB 540 Series (Rp.800.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Intel SSD 240GB 535 Series (Rp.1.930.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">Intel SSD 480GB S3100 Series (Rp.3.840.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="5">WDC 1TB SATA3 64MB Black (Rp.1.100.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="6">WDC 500GB SATA3 32MB Black (Rp.864.000)</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="found_site" style="color: lawngreen;">Casing yang mau dibeli</label>
                        <select class="form-control" name="case" id="found_site">
                            <option style="background-color: black; color:lawngreen;" value="1">-Pilih Casing-</option>
                            <option style="background-color: black; color:lawngreen;" value="2">Corsair Carbide SPEC(Rp.955.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="3">Corsair Obsidian 450X (Rp.1.735.000)</option>
                            <option style="background-color: black; color:lawngreen;" value="4">Corsair Graphite 760T (Rp.2.685.000)</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12 hidden">
                        <label for="specify">Please Specify</label>
                        <textarea class="form-control" id="specify" name=""></textarea>
                    </div>

                </fieldset>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" required value="" id="">
                                <p style="color: lawngreen;"> I accept the <a href="term.php" style="color: lawngreen;">terms and conditions</a>.</p>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary" style="background-color: lawngreen; font-weight: bold;">
                            Register
                        </button>

                    </div>
                </div>

            </form>
        </div>
    </div>

</body>

</html>
