<html>

<head>
    <title>Beranda</title>
    <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="icon" href="gambar/favicon.ico">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/java.js"></script>
    <style>
        .thumbnail {
            background-color: black;
        }
        
        h3 {
            color: white;
        }
        
        p {
            color: white;
            text-align: justify;
        }
        
        .btn {
            background-color: transparent;
            color: white;
            font-weight: bolder;
            border: 2px solid white;
        }
        
        .btn:hover {
            background-color: white;
            color: black;
            border: 2px solid black;
        }
        
        .gopro {
            width: 90%;
            margin-left: 40px;
        }
        
        #myphoto {
            width: 500px;
            height: 290px;
        }
        
        .progop1 {
            padding-top: 50px;
            padding-left: 150px;
            margin-left: 40%;
            width: 700px;
            font-family: segoe ui light;
            text-align: justify;
            color: white;
        }

    </style>
</head>

<body style="background-color:black;">
   <div class="header">
        <img src="gambar/logo1.png">
    </div>
    <div class="header2">
        <ul>
            <li><a href="index.php" class="dropbtn">Beranda</a></li>
            <li class="dropdown">
                <a href="#" class="dropbtn">Produk Kami</a>
                <div class="dropdown-content">
                    <a href="nvidia.php">VGA</a>
                    <a href="intel.php">Processor</a>
                    <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group" style="margin-top:15px;">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                        </div>
                        <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                    </form>
                </div>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="gopro">
        <h1 style="margin-left:200px; color: white;">GEFORCE GTX 10 SERIES GPUS HAVE COME TO LAPTOPS</h1>
        <div class="gopro1">
            <img id="myphoto" src="gambar/kolom.png" style="float: left;">
        </div>

        <div class="progop1">

            <h4>Gamers can now experience the power and performance of GeForce GTX 10-Series GPUs in a mobile form factor. These notebooks give you everything you need to unleash your gaming dominance by turning your mobile rig into a sleek, powerful gaming weapon.</h4>
            <br>
            <br>
            <h4 style="color: lawngreen; cursor:pointer;">EXPLORE GEFORCE GTX 10-SERIES NOTEBOOKS</h4>
        </div>
    </div>
    <br>
    <br>
    <br>
    <hr style="background-color:white; width: 1200px; margin-left: 70px;">
    <div class="row" style="padding-left:140px; margin-top:150px">
        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/nvidia1.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 1080 Ti</h3>
                    <p>NVIDIA's new flagship GeForce GTX 1080Ti is the most advanced gaming graphics card ever created.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/nvidia1.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 1080</h3>
                    <p>The GeForce GTX 1080 graphics card delivers the incredible speed and power of NVIDIA Pascal™</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx1070.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 1070</h3>
                    <p>Take on today's most challenging, graphics-intensive games without missing a beat. #GameReady</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx1060.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 1060</h3>
                    <p>the GeForce GTX 1060 delivers brilliant performance that opens the door to virtual reality and beyond.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx1050.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 1050</h3>
                    <p>NVIDIA Game Ready technologies that let every gamer experience the latest titles in their full glory.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left:140px;">
        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx980.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 980</h3>
                    <p>Powered by NVIDIA Maxwell architecture, these graphics cards delivers incredible performance.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/nvidia1.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 980 Ti</h3>
                    <p>GeForce GTX 980 is designed to solve the most complex lighting and graphical challenges in visual computing</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx970.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 970</h3>
                    <p>The GeForce® GTX 970 is a high-performance graphics card designed for serious gaming. It features technologies.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx960.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 960</h3>
                    <p>Experience next-generation gaming with GeForce® GTX 960—the perfect upgrade to the advanced performance.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/gtx950.jpg">
                <div class="caption">
                    <h3>Nvidia GTX 950</h3>
                    <p>The GeForce GTX 950 is a true gaming GPU designed for every PC gamer. Transform your PC into a gaming rig.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>



    </div>
    <div id="footer">
        <img src="gambar/logo1.png" class="logof">
        <p class="pf"> &copy; Copyright 2016</p>

    </div>
</body>

</html>
