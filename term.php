<html>

<head>
    <title>Beranda</title>
    <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
     <link rel="icon" href="gambar/favicon.ico">
    <link href="style-newSites.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/java.js"></script>
    <style>
        .ul{
            margin-left: 150px;
            margin-right: 150px;
            text-align: justify;
            color: white; 
        }
       
    </style>
</head>

<body>
    <div class="header">
        <img src="gambar/logo1.png">
    </div>
    <div class="header2">
        <ul>
            <li><a href="index.php" class="dropbtn">Beranda</a></li>
            <li class="dropdown">
                <a href="#" class="dropbtn">Produk Kami</a>
                <div class="dropdown-content">
                    <a href="nvidia.php">VGA</a>
                    <a href="intel.php">Processor</a>
                    <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group" style="margin-top:15px;">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                        </div>
                        <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                    </form>
                </div>
        </ul>
    </div>
    
   <br>
    <br>
    <div class="ul">
<h2 style="border-bottom: 1px solid white;">Terms And Condition</h2>
    <ul>
    <li>Website kami diupdate setiap hari kerja untuk mengikuti perubahan rate USD terhadap Rupiah.</li>
        <li>Ketersediaan barang & harga yang dicantum di website kami bisa berubah tanpa pemberitahuan dahulu. Disarankan konfirmasi melalui telepon atau YM untuk menge-cek kepastian stock barang dan harga ter-update sebelum melakukan pemesanan / transaksi. </li>
        <li>Semua barang yang dijual adalah barang baru dan bergaransi distributor resmi untuk masing-masing produk. </li>
        <li>Pembelian bisa langsung datang ke PASCAL Electronics atau menggunakan jasa pengiriman TIKI JNE atau ekspedisi lain nya. Ongkos kirim ditanggung oleh pembeli. <font color="lawngreen">Pada saat kesepakatan transaksi, jumlah berat barang yang diminta oleh staff marketing kami adalah perkiraan awal, karena yang menentukan berat akhir barang adalah pihak ekspedisi, jadi kepastian final ongkos kirim sesuai yang tertera di resi pengiriman ekspedisi.</font> Apabila terjadi kekurangan ongkos kirim, maka pihak Pascal Electronics berhak untuk meminta kekurangan sejumlah yang tertera di resi, dan apabila terjadi kelebihan ongkos kirim yang sudah dibayar ke pihak Pascal Electronics, maka menjadi hak dan kewajiban customer untuk meminta refund selisih kelebihannya sesuai yang tertera di resi dengan melakukan konfirmasi ke pihak marketing kami.</li>
        <li><font color="lawngreen">Apabila terjadi kehilangan barang atau keterlambatan pengiriman setelah Enter Komputer mendapatkan bukti resi pengiriman dari JNE / ekspedisi lainnya yang terjadi karena kesalahan pengiriman oleh kurir atau petugas dari pihak JNE / ekspedisi lainnya adalah DILUAR TANGGUNG JAWAB Enter Komputer </font>baik dengan ada atau tidaknya informasi dari Staff marketing kami sebelumnya pada saat kesepakatan transaksi. Untuk menghindari kehilangan barang, kita merekomendasikan agar menggunakan asuransi dari ekspedisi dengan biaya sebesar (0.2% dari harga barang ditambah biaya admin Rp 5.000) Contoh, harga barang 10 Juta, besar biaya asuransi = 10.000.000 x 0.2% = 20.000 + Biaya admin 5.000 = Rp 25.000.</li>
        <li>Jika terjadi kerusakan barang setelah beberapa waktu dan masih tercover oleh garansi, customer dapat membawa langsung ke distributor atau mengirimkan kembali ke Enter Komputer agar kita bantu teruskan ke pihak distributor. <font color="lawngreen">Lama tidak nya proses claim tergantung dari pihak distributor sepenuhnya. Customer tidak dapat memaksa pihak Enter Komputer untuk mempercepat proses claim karena Enter Komputer hanya menjembatani user dengan distributor.</font> Enter Komputer tidak mengenakan charge ke customer untuk proses claim selain biaya yang diminta dari distributor (kalau ada, tergantung kondisi kerusakan) untuk upgrade produk / penggantian spare part. Ongkos kirim ditanggung oleh customer sepenuhnya.</li>
    </ul>
     
     
        </div>
    </body></html>