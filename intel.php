<html>

<head>
    <title>Beranda</title>
    <script type="text/javascript" src="js/jquery-2.2.3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="icon" href="gambar/favicon.ico">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/java.js"></script>
    <style>
        .thumbnail {
            background-color: black;
        }
        
        h3 {
            color: white;
        }
        
        p {
            color: white;
            text-align: justify;
        }
        
        .btn {
            background-color: white;
            color: black;
            font-weight: bolder;
        }
        
        .btn:hover {
            background-color: black;
            border: 2px solid white;
        }
        
        .gopro {
            width: 90%;
            margin-left: 40px;
        }
        
        #myphoto {
            width: 600px;
            height: 230px;
            margin-left: 60px;
        }
        
        .progop1 {
            padding-top: 50px;
            padding-left: 150px;
            margin-left: 300px;
            width: 900px;
            font-family: segoe ui light;
            text-align: justify;
            color: white;
        }

    </style>
</head>

<body style="background-color:black;">
   <div class="header">
        <img src="gambar/logo1.png">
    </div>
    <div class="header2">
        <ul>
            <li><a href="index.php" class="dropbtn">Beranda</a></li>
            <li class="dropdown">
                <a href="#" class="dropbtn">Produk Kami</a>
                <div class="dropdown-content">
                    <a href="nvidia.php">VGA</a>
                    <a href="intel.php">Processor</a>
                    <li><a href="form.php" class="dropbtn">Pembelian</a></li>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group" style="margin-top:15px;">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                        </div>
                        <button type="submit" class="btn btn-default " style="margin-top:15px;">Submit</button>
                    </form>
                </div>
        </ul>
    </div>
    <br>
    <br>
    <br>
    <div class="gopro">

        <div class="gopro1">
            <img id="myphoto" src="gambar/intelcore.png" style="float: left;">
        </div>

        <div class="progop1">

            <h1 style="margin-left:200px; color: white;">Kini Saatnya untuk 
PC yang Lebih Cepat dan Pintar.</h1>
            <br>
            <br>

        </div>
    </div>
    <br>
    <br>
    <br>
    <hr style="background-color:white; width: 1200px; margin-left: 70px;">
    <div class="row" style="padding-left:140px; margin-top:150px">
        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/intel7.png">
                <div class="caption">
                    <h3>Intel Core i7 7700K</h3>
                    <p>7th Generation Intel Core is the newest family of processors for 2 in 1s and laptops. 7th Gen </p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/intel5.png">
                <div class="caption">
                    <h3>Intel Core i5 7200</h3>
                    <p>The Intel Core i5-7200U is an upcoming dual-core processor of the Kaby Lake architecture.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/intel3.png">
                <div class="caption">
                    <h3>Intel Core i3 7100</h3>
                    <p>Intel® Core™ i3, featuring smooth streaming, and immersive, full-screen 4K.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/intelm3.png">
                <div class="caption">
                    <h3>Intel Core M3</h3>
                    <p>Made for 2 in 1 devices, the Intel® Core™ M processor supports mobile work without compromising visuals.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-2">
            <div class="thumbnail">
                <img src="gambar/intelm7.jpg">
                <div class="caption">
                    <h3>Intel Core M7</h3>
                    <p>The Intel Core m7-6Y75 is the fastest processor in the power-efficient Core M chip family, oftentimes used in ultra-thin.</p>
                    <p><a href="form.php" class="btn btn-primary" role="button">Buy!</a></p>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <img src="gambar/logo1.png" class="logof">
        <p class="pf"> &copy; Copyright 2016</p>

    </div>
</body>

</html>
