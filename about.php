<html>
	<head>
		<title>About Us</title>
		
        
        <!--  CSS FILES  -->
		<link href="style-newSites.css" rel="stylesheet" />
		<link rel="shortcut icon" href="../i/favicon.ico" type="image/x-icon">
		
		
		<style type='text/css'>
			@-ms-viewport {width: device-width;}
			@-o-viewport {width: device-width;}
			@viewport {width: device-width;}
		</style>
		
		
		
		
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		
		
	</head>
	
	<body>
        
<section class="the-team">
				<a name="team"></a>
				<div class="page-w">
					<div class="team-member">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="bear.jpg" alt="" width="121" height="121" />
								</div>
								<div class="back">
									<img src="../i/patrice.png" alt="" width="121" height="121" />
								</div>
							</div>
						</div>
						
						<h1>Moch Rizky J</h1>
						
						<h2>Creative Direction and strategy</h2>
						
						
					</div> <!-- end team-member -->
					
					<div class="team-member">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="bird.jpg" alt="" width="121" height="121" />
								</div>
								<div class="back">
									<img src="../i/desi.png" alt="" width="121" height="121" />
								</div>
							</div>
						</div>
						
						<h1>Muhammad Rafi</h1>
						
						<h2>Project Director</h2>
						
					</div> <!-- end team-member -->
					
					<div class="team-member">
						<div class="flip-container">
							<div class="flipper">
								<div class="front">
									<img src="mountain-lion.jpg" alt="" width="121" height="121" />
								</div>
								<div class="back">
									<img src="../i/martin.png" alt="" width="121" height="121" />
								</div>
							</div>
						</div>
						
						<h1>Sandi Setiawan</h1>
						
						<h2>Graphic/UI Design</h2>

					</div> <!-- end team-member -->
					
					
					<div class="clear"></div>
				</div>
			</section> <!-- END THE-TEAM -->
</body>
</html>